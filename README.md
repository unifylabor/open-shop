## Open Shop - Unify Labor

> Public facing Unify Labor App.


### Contents

- [Scope](#scope)
- [Methodology](#methodology)
- [Checks & Balances](#checks)
- [Setup](#setup)
- [Assets](#assets)
- [Documentation](#documentation)
- [Contribute](#contribute)
- [Questions & Media Contact](#questions)
- [Support](#support)


### Scope

### Methodology

### Checks & Balances

### Setup

### Assets

### Documentation

### Contribute

Contributions welcome. Please read the guidelines & be patient (we're all volunteers).

### Questions & Media Contact

Contributions welcome. Please read the guidelines & be patient (we're all volunteers).

### Support

* Submit Bugs:  https://gitlab.com/unifylabor/open-shop/issues
* Project Page:  https://gitlab.com/unifylabor/open-shop

##### License

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0)
